import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import{map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ModelService {
  baseurl : string = "http://bf66c078bb54.ngrok.io"
  constructor(private http : HttpClient) { }

  getResult(){
    let objet ={ Open: null,Close : null, date: null};
    return this.http.get(`${this.baseurl}/predict`).pipe(
      map(
      (res : any)=>{
        objet.Open = res.Open ;
        objet.Close = res.Close ;
        objet.date = "2020-07-02";
        console.log("objet",objet);
        return objet;
    }

    ))

  }

}
