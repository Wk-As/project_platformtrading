import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockAcheteComponent } from './stock-achete.component';

describe('StockAcheteComponent', () => {
  let component: StockAcheteComponent;
  let fixture: ComponentFixture<StockAcheteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockAcheteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockAcheteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
