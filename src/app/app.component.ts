import { Component, OnInit } from '@angular/core';
import {Chart} from 'Chart.js' ;

import {MarketStatusService} from './market-status.service';
import {Observable} from 'rxjs';
import {MarketPrice} from './market-price';
import { AuthService } from './login/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title = 'PlatForm';

  constructor(private auth: AuthService) {}
  isConected() {
    return this.auth.getIslogin() ;
  }
}


