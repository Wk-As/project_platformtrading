import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {map, take} from 'rxjs/operators';
import { lineChartData } from './line-chart/line-chart.component';
import { ModelService } from './model.service';




@Injectable({
  providedIn: 'root'
})
export class HttpHomeService {

  constructor(private http: HttpClient,private model : ModelService) { }
  URL = "http://localhost:5000"
  objet = { date : "", open : "" , close :""}
  getLineData(stock_name : string){
    let date = [];
    let open = [];
    let close = [];
    return this.http.get(`${this.URL}/api/market/${stock_name}`)
    .pipe(
      map(
        (data : any) =>{
          data.forEach(element => {
            date.push(element.date);
            open.push(element.open);
            close.push(element.close);
          });
          let stock = {
            labels : date,
            open : open,
            close : close
          };

          return stock;

        }
      ));
  };



}
