import { Component, OnInit } from '@angular/core';
import {Chart} from 'chart.js';
import { HttpHomeService } from '../http-home.service';
import { ActivatedRoute } from '@angular/router';
import { ModelService } from '../model.service';


@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {
  stock_name : string;
  private objet :any;
  constructor(private httpService: HttpHomeService, private route : ActivatedRoute,private model : ModelService) { }

  theData: any ;

  getLineData(stock_name :string) {
    this.httpService.getLineData(stock_name)
    .subscribe( (data :any)=> {
      this.model.getResult().subscribe((res : any)=>{
        data.labels.push(res.date);
        data.open.push(res.Open);
        data.close.push(res.Close);
        console.log(res);
        console.log(data);
    });

      let  myLine = new Chart('myline', {
      type: 'line',
      data: {
          labels: data.labels,
          datasets: [
            {
              label: `Stocks close for ${stock_name}`,
              data: data.close,//[12,56,423,65,98],
              backgroundColor: [
                  ' rgba(91, 192, 222,0.1)',
              ],
              borderColor: [
                'rgb(91, 192, 222)',
            ],
            pointBackgroundColor:[

              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(46, 46, 46, 0.651)',
              'rgb(81, 192, 202)',

            ],

              borderWidth: 1
          },
          {
            label: `Stocks for open ${stock_name}`,
            data: data.open,//[12,56,423,65,98],
            backgroundColor: [
                '#b5215548',
            ],
            borderColor: [
              'rgb(218, 24, 24)',
          ],
          pointBackgroundColor:[

            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(46, 46, 46, 0.651)',
            'rgb(200, 20, 20)',

          ],
            borderWidth: 1
        }
        ]
      },
      options: {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
              yAxes: [{
                  gridLines: {
                    display: false
                  },
                  ticks: {
                      beginAtZero: true
                  }
              }],
              xAxes: [{
                gridLines: {
                  display: true
                }
            }],

          }
      }
  });

    //end chart
    } );
  }

  get_(){
    return this.model.getResult().subscribe((res : any)=>{
      this.objet.date ="2020-06-27"
      this.objet.open =res.Open;
      this.objet.close =res.Close;
  });
  }



  ngOnInit(): void {

    this.route.params.subscribe((param)=>{
      console.log(param);
      this.stock_name = param.name;
      console.log(this.stock_name);
    })
    this.getLineData(this.stock_name) ;






  }

}

export interface lineChartData {
  data : number[] ,
  labels : string[] ,
}
