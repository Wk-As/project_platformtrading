import { Injectable } from '@angular/core';
import { BuyStockService } from './buy-stock.service';
import { MarketStatusService } from './market-status.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private ghani: User = {name: 'ghani' , email:"abdelghani.kad@gmail.com" , solde : 1000000};

  getUser() {
    return this.ghani ;
  }
  firstSet(solde:number){
    this.ghani.solde = solde;
  }

  setSolde(solde:number) {
    this.market_ser.getSolde().subscribe((res : any)=>{
      let budget = res.budget;
      this.firstSet(budget);
      console.log("user solde",this.ghani.solde);
    });
  }





  constructor(private market_ser : MarketStatusService) { }
}

export interface User{
  name: string;
  email: string;
  solde: number ;

}
