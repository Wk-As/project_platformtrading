import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SideNavComponent } from './side-nav/side-nav.component';
import { CompCardsComponent } from './comp-cards/comp-cards.component';
import { StockAcheteComponent } from './stock-achete/stock-achete.component';
import { VisualChartComponent } from './visual-chart/visual-chart.component';
import { AuthGuard } from './login/auth.guard';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  {path : 'login', component : LoginComponent},
  { path : '', component : CompCardsComponent , canActivate: [AuthGuard]},
  {path: 'my-stock' , component: StockAcheteComponent , canActivate: [AuthGuard]},
  {path : 'chart/:name' , component: VisualChartComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
