import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BuyStockService {
  readonly ROOT_URL: string;

  get(uri : string) {
    return this.http.get(`${this.ROOT_URL}/${uri}`);
  }
  post(uri : string, payload : Object){
    return this.http.post(`${this.ROOT_URL}/${uri}`, payload);
  }
  patch(uri : string, payload : Object){
    return this.http.patch(`${this.ROOT_URL}/${uri}`, payload);
  }
  delete(uri : string){
    return this.http.delete(`${this.ROOT_URL}/${uri}`);
  }

  private stocks: BuyStock[] = [
    {name : 'GOOG' , prix : 127},
    {name : 'AMZN' ,  prix :  100},
    {name : 'SONI' ,  prix :  200},
    {name : 'APPL' ,  prix :  266},
  ];

  getStocks() {
    return this.get('myStocks');
  }
  By(stock:number){
    console.log("stock by",stock);
    this.user.setSolde(stock);
  }
  buyStock(stock:any) {
    this.user.setSolde(stock.prix) ;
    return this.post('buy',stock);
  }
  sellStock(stock:any){
    this.user.setSolde(stock.prix) ;
    return this.delete(`sell/${stock}`)
  }

  getSolde(){
    return this.get('/getBudget');
  }

  login(user : any){
    return this.post('/login',user);
  }


  constructor(private user: UserService, private http : HttpClient) {
    this.ROOT_URL = 'http://localhost:5000';
   }



}

export interface BuyStock {
  name: string;
  prix: number;
}
