import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
// import { HeaderServiceService } from '../header-service.service';
import { Router } from '@angular/router';
import { UserService, User } from '../user.service';
// import { AuthServiceService } from '../login/auth-service.service';



@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit{

  Title = 'Home';
  Icon = 'Home';
  theUser: User ;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,
              // private headerservice : HeaderServiceService,
              private Router : Router,
              private user: UserService
              // private auth : AuthServiceService
              ) {
                // this.headerservice.title.subscribe(title =>{
                //   this.Title =title;
                // });
                // this.headerservice.icon.subscribe(icon =>{
                //   this.Icon =icon;
                // });
              }

  SetTitle(name : string){
    this.Title=name;
  }
  ngOnInit(): void {

    this.theUser = this.user.getUser() ;

  }
  goDemande(){
    this.Router.navigateByUrl('demande');
  }

  goOperations() {
    this.Router.navigateByUrl('operations');
  }
  disconnect(){
    // this.auth.logout();

  }
}
