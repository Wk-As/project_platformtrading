from flask import Flask, jsonify , request
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_socketio import SocketIO,emit,send
import numpy as np
import datetime as dt
import json
import pymysql
import data as dts


app= Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app,cors_allowed_origins="*")

pymysql.install_as_MySQLdb()


app.config['SQLALCHEMY_DATABASE_URI']='mysql://root:@localhost/trade'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db=SQLAlchemy(app)
cors = CORS(app,resources={r"/*":{"origins":"*"}})
class User(db.Model):
	__tablename__ = 'user'
	id = db.Column("id", db.Integer, primary_key=True)
	username = db.Column("username",db.String(100))
	password = db.Column("password",db.String(100))
	budget = db.Column("budget", db.Float)
	stocks = db.relationship('Bought_stock', backref='owner')

	def __init__(self, id, username, password, budget):
		self.id = id
		self.username = username
		self.password = password
		self.budget = budget

class Bought_stock(db.Model):
	id = db.Column("id", db.Integer, primary_key=True)
	name = db.Column("name",db.String(100))
	price = db.Column("price", db.Float)
	user_id = db.Column("user_id",db.Integer,db.ForeignKey('user.id'))

	def __init__(self,  name, price, user_id):
		self.name = name
		self.price = price
		self.user_id = user_id

@app.route('/',methods=['GET'])
def test():
	return jsonify({'message':'It works!'})


@app.route('/login',methods=['POST'])
def login():
	user1 = User.query.filter_by(id=1).first()
	if request.json['username'] == user1.username and request.json['password'] == user1.password :
		return jsonify({})
	else :
		return jsonify({"message": "Acces denied!"}), 403

@app.route('/myStocks',methods=['GET'])
def get_stocks():
	prices=[]
	names=[]
	ids=[]
	stocks = Bought_stock.query.filter_by(user_id=1).all()
	for x in stocks:
		prices.append(x.price)
	print("prices",prices)
	for x in stocks:
		names.append(x.name)
	for x in stocks:
		ids.append(x.id)
	return json.dumps([{'id':id,'name': name, 'price': price} for id,name, price in zip(ids,names, prices)])

@app.route('/myStocksById/<int:id>',methods=['GET'])
def get_stocks_by_id(id):
  stock = Bought_stock.query.filter_by(user_id=1, id=id).first()
  return jsonify({'id':stock.id, 'name': stock.name, 'price': stock.price})

@app.route('/myStocks/<string:stock_name>',methods=['GET'])
def get_one_stock(stock_name):
	prices=[]
	num=[]
	stocks = Bought_stock.query.filter_by(user_id=1, name=stock_name).all()
	for x in stocks:
		prices.append(x.price)
	print("prices",prices)
	for x in stocks:
		num.append(x.id)
	stock_dictionary = dict(zip(num, prices))
	return jsonify({'stocks':stock_dictionary})

@app.route('/sell/<string:stock_name>',methods=['DELETE'])
def sell(stock_name):
	user1 = User.query.filter_by(id=1).first() #Choix de l'utilisateur1
	stock = Bought_stock.query.filter_by(user_id=1, name=stock_name).first()
	user1.budget = user1.budget + stock.price
	db.session.delete(stock)
	db.session.commit()
	return jsonify({})

@app.route('/sellById/<int:id>',methods=['DELETE'])
def sell_by_id(id):
	user1 = User.query.filter_by(id=1).first() #Choix de l'utilisateur1
	stock = Bought_stock.query.filter_by(user_id=1, id=id).first()
	user1.budget = user1.budget + stock.price
	db.session.delete(stock)
	db.session.commit()
	return jsonify({})

@app.route('/buy',methods=['POST'])
def buy():
	user1 = User.query.filter_by(id=1).first()
	if user1.budget >= request.json['price'] :
		bought_stock = Bought_stock(name= request.json['name'], price= request.json['price'], user_id=1)
		print(user1.budget,' ',bought_stock.price)
		sd1 = user1.budget
		user1.budget = sd1 - bought_stock.price
		db.session.add(bought_stock)
		db.session.commit()
		return jsonify({"avant" :sd1 , "stock" : bought_stock.price, "apres":sd1 - bought_stock.price})
	else :
		return jsonify({"message": "Not enough money!"}), 403


@app.route('/changeBudget',methods=['PUT'])
def change_budget():
	user1 = User.query.filter_by(id=1).first() #Choix de l'utilisateur1
	user1.budget = request.json['budget']
	db.session.commit()
	return jsonify({'budget':user1.budget})

@app.route('/getBudget',methods=['GET'])
def get_Budget():
	user1 = User.query.filter_by(id=1).first() #Choix de l'utilisateur1
	return jsonify({'budget':user1.budget})



marketPositions = [
    {"date": "10-05-2012", "close": 68.55, "open": 74.55},
    {"date": "09-05-2012", "close": 74.55, "open": 69.55},
    {"date": "08-05-2012", "close": 69.55, "open": 62.55},
    {"date": "07-05-2012", "close": 62.55, "open": 56.55},
    {"date": "06-05-2012", "close": 56.55, "open": 59.55},
    {"date": "05-05-2012", "close": 59.86, "open": 65.86},
    {"date": "04-05-2012", "close": 62.62, "open": 65.62},
    {"date": "03-05-2012", "close": 64.48, "open": 60.48},
    {"date": "02-05-2012", "close": 60.98, "open": 55.98},
    {"date": "01-05-2012", "close": 58.13, "open": 53.13},
    {"date": "30-04-2012", "close": 68.55, "open": 74.55},
    {"date": "29-04-2012", "close": 74.55, "open": 69.55},
    {"date": "28-04-2012", "close": 69.55, "open": 62.55},
    {"date": "27-04-2012", "close": 62.55, "open": 56.55},
    {"date": "26-04-2012", "close": 56.55, "open": 59.55},
    {"date": "25-04-2012", "close": 59.86, "open": 65.86},
    {"date": "24-04-2012", "close": 62.62, "open": 65.62},
    {"date": "23-04-2012", "close": 64.48, "open": 60.48},
    {"date": "22-04-2012", "close": 60.98, "open": 55.98},
    {"date": "21-04-2012", "close": 58.13, "open": 53.13}
  ];
marketPositions1 = [
    {"date": "10-05-2012", "close": 680.55, "open": 74.55},
    {"date": "09-05-2012", "close": 749.55, "open": 69.55},
    {"date": "08-05-2012", "close": 690.55, "open": 62.55},
    {"date": "07-05-2012", "close": 62.55, "open": 56.55},
    {"date": "06-05-2012", "close": 56.55, "open": 59.55},
    {"date": "05-05-2012", "close": 59.86, "open": 65.86},
    {"date": "04-05-2012", "close": 62.62, "open": 65.62},
    {"date": "03-05-2012", "close": 64.48, "open": 60.48},
    {"date": "02-05-2012", "close": 60.98, "open": 55.98},
    {"date": "01-05-2012", "close": 58.13, "open": 53.13},
    {"date": "30-04-2012", "close": 68.55, "open": 74.55},
    {"date": "29-04-2012", "close": 74.55, "open": 69.55},
    {"date": "28-04-2012", "close": 69.55, "open": 62.55},
    {"date": "27-04-2012", "close": 62.55, "open": 56.55},
    {"date": "26-04-2012", "close": 56.55, "open": 59.55},
    {"date": "25-04-2012", "close": 59.86, "open": 65.86},
    {"date": "24-04-2012", "close": 62.62, "open": 65.62},
    {"date": "23-04-2012", "close": 64.48, "open": 60.48},
    {"date": "22-04-2012", "close": 60.98, "open": 55.98},
    {"date": "21-04-2012", "close": 58.13, "open": 53.13}
  ];

@app.route('/api/market/<string:stock_name>',methods=['GET'])
def gets_stocks(stock_name):
    return jsonify(dts.stacks(stock_name))

@app.route('/stocks/<string:stock_name>',methods=['GET'])
def get_price(stock_name,):
    objet = dts.get_price(stock_name, "2020-07-02")
    return jsonify(objet)

# @app.route('/stocks',methods=['GET'])
# def get_prices():
#     Stocks =['AAPL', 'IBM', 'AMZN', 'TWTR', 'GOOG']
#     result =[]
#     for i in Stocks :
#         result.append(get_price(i))
#     return result

@socketio.on('connection')
def func(message):
    print('received message: ' + message)


@socketio.on('market')
def setInterval() :
	emit('market', marketPositions1[0],3000)


if __name__ == '__main__':
	socketio.run(app)
