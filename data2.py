
# P54AJS51K7LMVGQ3

import requests
import pandas as pd
import datetime
import matplotlib.pyplot as plt
from matplotlib import style
import numpy as np

def get_price(stock):
    style.use('fivethirtyeight')
    data=requests.get('https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={}&outputsize=compact&apikey=P54AJS51K7LMVGQ3'.format(stock))
    data=data.json()
    data=data['Time Series (Daily)']
    df=pd.DataFrame(columns=['date','open','high','low','close','volume'])
    # df=pd.DataFrame(columns=['open','high','low','close','volume'])
    for d,p in data.items():
        date=datetime.datetime.strptime(d,'%Y-%m-%d')
        data_row=[date,float(p['1. open']),float(p['2. high']),float(p['3. low']),float(p['4. close']),int(p['5. volume'])]
       
        df.loc[-1,:]=data_row
        df.index=df.index+1
    df=df.sort_values('date')
    df=df.loc[99,'close']
    df = { "name" : stock , "price" : df }
    return df

def stacks(stock):
    meta = []
    style.use('fivethirtyeight')
    data=requests.get('https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={}&outputsize=compact&apikey=P54AJS51K7LMVGQ3'.format(stock))
    data=data.json()
    data=data['Time Series (Daily)']
    # df=pd.DataFrame(columns=['open','high','low','close','volume'])
    for d,p in data.items():
        date=datetime.datetime.strptime(d,'%Y-%m-%d')
        json_={"date" : date , "open" : float(p['1. open']) , "close" : float(p['4. close']) }
        meta.append(json_)
    print(meta)
    return meta


if __name__ == '__main__':
    stacks("IBM")